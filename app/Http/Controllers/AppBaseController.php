<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Konatra APIs",
 *     version="1.0.0",
 *   ),
 *     security={
 *        {
 *            "passport-swaggervel_auth": {"*"}
 *        }
 *     },
 * )
 *
 * @SWG\SecurityScheme(
 *   securityDefinition="passport-swaggervel_auth",
 *   description="OAuth2 grant provided by Laravel Passport",
 *   type="oauth2",
 *   authorizationUrl="http://komnatra.local/oauth/token",
 *   tokenUrl="http://komnatra.local/oauth/token",
 *   flow="password",
 *   scopes={
 *     *
 *   }
 * ),
 *
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
