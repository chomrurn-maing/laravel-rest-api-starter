# Laravel Khmer Express POS System

### 1. Install Project
- Clone `git clone git@gitlab.com:mulitcaps/komnatra-server.git`
- Run `cd laravel-rest-api-starter`
- Run `composer install`
- Create VHost with name `your-host-name.local`
    - Host directory must `root-directory\public`
    
- Create Database
    - Name `database_name`
    - Character set `utf8`
    - Collation `utf8_general_ci`
    
    - Duplicate `.env-example` name `.env` in root directory and modify
    - APP_URL=YOUR_VIRTUAL_HOST (You cannot run with localhost:xxxx, It works only virtual host)
    - DB_DATABASE=YOUR_DATABASE_NAME
- Change `App\Http\Controllers\AppBaseController`:
    - `authorizationUrl="your-host-name.local/oauth/token"`
    - `tokenUrl="your-host-name.local/oauth/token"`
- Run `php artisan config:cache`
- Run `php artisan migrate --seed`
- Run `php artisan key:generate`
- Run `php artisan config:cache`
- Run `php artisan passport:install`

### 2. The must before starting this project

- See the [Laravel Generator](https://labs.infyom.com/laravelgenerator/docs/5.8/introduction) documents

### 3. App Info
- Login Info:
    - Username: `administrator`
    - Password: `password`
    
### 4. Enjoy
- For API doc just append `/api/docs` next to your base URL.



