<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PassportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'name'      => 'Laravel Personal Access Client',
            'secret'    => 'R0Tt3oXQbBpKAFbHSagkQpZp5qLUaqJuI2B37L3K',
            'redirect'  => 'khmer-express.local',
            'personal_access_client'    => 1,
            'password_client'           => 0,
            'revoked'                   => 0,
        ]);

        DB::table('oauth_clients')->insert([
            'name'      => 'Laravel Password Grant Client',
            'secret'    => 'dwMLTIOCbhDT0ZoS8SVMdOOkL6MteCg6yHu4WPnq',
            'redirect'  => 'khmer-express.local',
            'personal_access_client'    => 0,
            'password_client'           => 1,
            'revoked'                   => 0,
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => 1
        ]);
    }
}
